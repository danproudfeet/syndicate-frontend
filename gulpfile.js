var gulp = require('gulp');
// var sass = require('gulp-sass');
var sass = require('gulp-ruby-sass');
var prefix = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var csso = require('gulp-csso');
var browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
	browserSync.init({
		server: "./"
	});
	gulp.watch('**/*.html').on('change', browserSync.reload);
});

gulp.task('sass', function() {
	return sass('./css/**/*.scss')
	.on('error', sass.logError)
	// gulp.src('./css/**/*.scss')
	// .pipe(sass().on('error', sass.logError))
	.pipe(prefix({
			browsers: ['last 2 versions'],
			cascade: true
	}))
	.pipe(gulp.dest('./css/'))
	.pipe(browserSync.stream());
})

gulp.task('uglify', function() {
	return gulp.src('./js/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('./js/min'));
})

gulp.task('csso', ['sass'], function() {
	return gulp.src('./css/style.css')
		.pipe(csso())
		.pipe(gulp.dest('./css/min'))
})

gulp.task('compress', ['uglify', 'csso']);

gulp.task('default', ['sass', 'browser-sync'], function() {
	gulp.watch('css/**/*.scss', ['sass']);
});